ETechUptake New Core Documentation
==================================


Contents:

.. toctree::
   :maxdepth: 2

   customer
   main
   manager
   structure
   tech
   time
   utility


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

